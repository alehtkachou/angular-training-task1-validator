app.controller('addController', ['$scope', '$state', 'listService', function ($scope, $state, listService) {

    "use strict";

    $scope.submit = function (addForm) {
        $scope.submitted = true;

        if ($scope.form.$invalid) {
            return;
        }

        listService.submit(addForm);
        $state.go("listsView");
    };

    $scope.submitted = false;
}]);