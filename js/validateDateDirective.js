app.directive('validateDate', function () {

    "use strict";

    var REGEX_DATE = /^\d{4}\/(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])$/;

    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {

            ctrl.$validators.date = function (viewValue) {

                if (REGEX_DATE.test(viewValue)) {
                    return true;
                }

                return false;
            };
        }
    };
});