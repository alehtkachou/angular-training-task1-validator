app.directive('validateInteger', function () {

    "use strict";

    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {

            ctrl.$validators.age = function (viewValue) {

                if (viewValue >= 18 && viewValue <= 65) {
                    return true;
                }

                return false;
            };
        }
    };
});