app.service('listService', function () {

    "use strict";

    var lists = [];

    var list = function (nameValue, ageValue, dateValue) {
        this.name = nameValue;
        this.age = ageValue;
        this.date = dateValue;
    };

    this.getLists = function() {
        return lists;
    };

    this.submit = function (addForm) {
        lists[lists.length] = new list(addForm.name, addForm.age, addForm.date);
    };
});