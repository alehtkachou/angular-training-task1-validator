app.config(function ($stateProvider, $urlRouterProvider) {

    "use strict";

    $urlRouterProvider.otherwise("/index");

    $stateProvider
        .state('index', {
            url: "/index",
            views: {
                "mainview": {
                    templateUrl: 'tpl/addView.tpl.html'
                }
            }
        })
        .state('addView', {
            url: "/add",
            views: {
                "mainview": {
                    templateUrl: 'tpl/addView.tpl.html'
                }
            }
        })
        .state('listsView', {
            url: "/lists",
            views: {
                "mainview": {
                    templateUrl: 'tpl/listsView.tpl.html'
                }
            }
        })
});