app.controller('listsController', ['$scope', 'listService', function ($scope, listService) {

    "use strict";

    $scope.lists = listService.getLists();
}]);