var express = require('express');

var app = express();

app.use(express.static(__dirname + '/'));

app.listen(3000, function () {
    console.info('Started server at 3000 port');
});